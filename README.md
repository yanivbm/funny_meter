# Jokes Regressor (Funny-Meter)

## Python 3.6

## Contributors:

- Axel Strubel
- Avi Barazani
- Yaniv Ben-Malka

## Summary

Using machine learning tools, we have built a supervised nlp-based jokes regressor, that gets an English joke as an input, creates various features that try to capture the sense of the joke, and predicts how funny it is, in a scale 0 to 100.

## Extended Summary

In ProjectSummary.pdf

## Dataset

A reddit jokes dataset, that contains more than 180,000 jokes and their score (votes). 

Our future intention is to use the scrap tweets from a pre-made list of 'funny' pages, and use the tweets to further train & test the model, in addition to some more twitter-based feature engineering.

## Files

#### Notebooks
- EDA
- Feature Engineering
- Feature Processing (Dimensionality Reduction)
- Modeling
#### Python
- feature_generator.py (Run the entire pipline for generating features for an input joke. Then run the selected model and get a score prediction)
#### Data
- reddit_jokes.json - Dataset currently in use
#### Twitter_API (Not in use)
- Get_Tweets.py - Tweets scraper
- tweets.csv - A sample dataset of scraped tweets
## Note about IPython notebooks presentation
- In order to be able to view IPython notebooks in BitBucket, first click on your Avatar then Bitbucket Settings then Marketplace. 
- You can search for or scroll down to the Notebook Viewer by Atlassian add-on and install it.
- If you then go back to the .ipynb file, you will see a drop-down menu at the top that says "Default File Viewer". "Ipython Notebook" will appear as an option after you install the Notebook Viewer add-on and refresh. 
- You can then toggle between the raw JSON and the Notebook views.
- Credit: https://stackoverflow.com/questions/47256074/how-to-display-ipynb-notebook-file-in-bitbucket